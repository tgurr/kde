# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require qmake sourceforge

SUMMARY="Qt Widgets for Technical Applications"
DESCRIPTION="
The Qwt library contains GUI Components and utility classes which are primarily useful for programs
with a technical background. Beside a 2D plot widget it provides scales, sliders, dials, compasses,
thermometers, wheels and knobs to control or display values, arrays, or ranges of type double.
"

LICENCES="Qwt-1.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    qt5 [[ description = [ Build against qt5 instead of qt4 ] ]]
"

DEPENDENCIES="
    build+run:
        qt5? (
            x11-libs/qtbase:5
            x11-libs/qtsvg:5
        )
        !qt5? ( x11-libs/qt:4[X(+)] )
"

DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

src_prepare() {
    edo cat > qwtconfig.pri <<-EOF
QWT_INSTALL_DOCS      = /usr/share/doc/${PNVR}
QWT_INSTALL_HEADERS   = /usr/$(exhost --target)/include/qwt6
QWT_INSTALL_LIBS      = /usr/$(exhost --target)/lib
QWT_INSTALL_PLUGINS   = /usr/$(exhost --target)/lib/qt$(option qt5 && echo 5 || echo 4)/plugins/designer
QWT_INSTALL_FEATURES  = /usr/$(exhost --target)/lib/qt$(option qt5 && echo 5 || echo 4)/features
QWT_CONFIG += QwtDll QwtPlot QwtWidgets QwtSvg QwtMathML QwtDesigner QwtPkgConfig
QWT_VERSION = ${PV}
EOF
}

src_configure() {
    if option qt5; then
        eqmake 5
    else
        eqmake 4
    fi
}


Upstream: yes

From 1637449008ee8f633d64891fe5aeba1689835797 Mon Sep 17 00:00:00 2001
From: Konstantin Tokarev <annulen@yandex.ru>
Date: Wed, 5 Jul 2017 17:31:36 +0300
Subject: [PATCH] [cmake] Add target argument to QTWEBKIT_GENERATE_MOC_* macros

qt5_generate_moc needs target argument to use correct include paths
and definitions in moc invocations. In particular this fixes build with
QT_NO_SSL defined.

Change-Id: I4d46de7845336fd3bee59d6632f67c0b0cac7019
---
 Source/WTF/wtf/PlatformQt.cmake |  4 ++--
 Source/WebCore/PlatformQt.cmake | 12 ++++++------
 Source/cmake/OptionsQt.cmake    | 18 ++++++++++++------
 3 files changed, 20 insertions(+), 14 deletions(-)

diff --git a/Source/WTF/wtf/PlatformQt.cmake b/Source/WTF/wtf/PlatformQt.cmake
index 32c8d2c70a0..684119455b3 100644
--- a/Source/WTF/wtf/PlatformQt.cmake
+++ b/Source/WTF/wtf/PlatformQt.cmake
@@ -4,7 +4,7 @@ list(APPEND WTF_SOURCES
 
     text/qt/StringQt.cpp
 )
-QTWEBKIT_GENERATE_MOC_FILES_CPP(qt/MainThreadQt.cpp qt/RunLoopQt.cpp)
+QTWEBKIT_GENERATE_MOC_FILES_CPP(WTF qt/MainThreadQt.cpp qt/RunLoopQt.cpp)
 
 list(APPEND WTF_SYSTEM_INCLUDE_DIRECTORIES
     ${Qt5Core_INCLUDE_DIRS}
@@ -33,7 +33,7 @@ if (UNIX AND NOT APPLE)
 
         qt/WorkQueueQt.cpp
     )
-    QTWEBKIT_GENERATE_MOC_FILES_CPP(qt/WorkQueueQt.cpp)
+    QTWEBKIT_GENERATE_MOC_FILES_CPP(WTF qt/WorkQueueQt.cpp)
 endif ()
 
 if (USE_GLIB)
diff --git a/Source/WebCore/PlatformQt.cmake b/Source/WebCore/PlatformQt.cmake
index c5466b6c788..2f0b9bd03a3 100644
--- a/Source/WebCore/PlatformQt.cmake
+++ b/Source/WebCore/PlatformQt.cmake
@@ -162,19 +162,19 @@ list(APPEND WebCore_SOURCES
     platform/text/qt/TextBreakIteratorInternalICUQt.cpp
 )
 
-QTWEBKIT_GENERATE_MOC_FILES_CPP(
+QTWEBKIT_GENERATE_MOC_FILES_CPP(WebCore
     platform/network/qt/DNSQt.cpp
     platform/qt/MainThreadSharedTimerQt.cpp
 )
 
-QTWEBKIT_GENERATE_MOC_FILES_H(
+QTWEBKIT_GENERATE_MOC_FILES_H(WebCore
     platform/network/qt/CookieJarQt.h
     platform/network/qt/QNetworkReplyHandler.h
     platform/network/qt/QtMIMETypeSniffer.h
 )
 
-QTWEBKIT_GENERATE_MOC_FILE_H(platform/network/qt/NetworkStateNotifierPrivate.h platform/network/qt/NetworkStateNotifierQt.cpp)
-QTWEBKIT_GENERATE_MOC_FILE_H(platform/network/qt/SocketStreamHandlePrivate.h platform/network/qt/SocketStreamHandleQt.cpp)
+QTWEBKIT_GENERATE_MOC_FILE_H(WebCore platform/network/qt/NetworkStateNotifierPrivate.h platform/network/qt/NetworkStateNotifierQt.cpp)
+QTWEBKIT_GENERATE_MOC_FILE_H(WebCore platform/network/qt/SocketStreamHandlePrivate.h platform/network/qt/SocketStreamHandleQt.cpp)
 
 if (COMPILER_IS_GCC_OR_CLANG)
     set_source_files_properties(
@@ -197,7 +197,7 @@ if (ENABLE_GAMEPAD_DEPRECATED)
     list(APPEND WebCore_SOURCES
         platform/qt/GamepadsQt.cpp
     )
-    QTWEBKIT_GENERATE_MOC_FILES_CPP(platform/qt/GamepadsQt.cpp)
+    QTWEBKIT_GENERATE_MOC_FILES_CPP(WebCore platform/qt/GamepadsQt.cpp)
 endif ()
 
 if (ENABLE_GRAPHICS_CONTEXT_3D)
@@ -374,7 +374,7 @@ if (USE_QT_MULTIMEDIA)
     list(APPEND WebCore_LIBRARIES
         ${Qt5Multimedia_LIBRARIES}
     )
-    QTWEBKIT_GENERATE_MOC_FILES_H(platform/graphics/qt/MediaPlayerPrivateQt.h)
+    QTWEBKIT_GENERATE_MOC_FILES_H(WebCore platform/graphics/qt/MediaPlayerPrivateQt.h)
 endif ()
 
 if (ENABLE_VIDEO)
diff --git a/Source/cmake/OptionsQt.cmake b/Source/cmake/OptionsQt.cmake
index 457fff566db..d756d0b8e0a 100644
--- a/Source/cmake/OptionsQt.cmake
+++ b/Source/cmake/OptionsQt.cmake
@@ -60,7 +60,10 @@ macro(QT_ADD_EXTRA_WEBKIT_TARGET_EXPORT target)
     endif ()
 endmacro()
 
-macro(QTWEBKIT_GENERATE_MOC_FILES_CPP)
+macro(QTWEBKIT_GENERATE_MOC_FILES_CPP _target)
+    if (${ARGC} LESS 2)
+        message(FATAL_ERROR "QTWEBKIT_GENERATE_MOC_FILES_CPP must be called with at least 2 arguments")
+    endif ()
     foreach (_file ${ARGN})
         get_filename_component(_ext ${_file} EXT)
         if (NOT _ext STREQUAL ".cpp")
@@ -68,12 +71,12 @@ macro(QTWEBKIT_GENERATE_MOC_FILES_CPP)
         endif ()
         get_filename_component(_name_we ${_file} NAME_WE)
         set(_moc_name "${CMAKE_CURRENT_BINARY_DIR}/${_name_we}.moc")
-        qt5_generate_moc(${_file} ${_moc_name})
+        qt5_generate_moc(${_file} ${_moc_name} TARGET ${_target})
         ADD_SOURCE_DEPENDENCIES(${_file} ${_moc_name})
     endforeach ()
 endmacro()
 
-macro(QTWEBKIT_GENERATE_MOC_FILE_H _header _source)
+macro(QTWEBKIT_GENERATE_MOC_FILE_H _target _header _source)
     get_filename_component(_header_ext ${_header} EXT)
     get_filename_component(_source_ext ${_source} EXT)
     if ((NOT _header_ext STREQUAL ".h") OR (NOT _source_ext STREQUAL ".cpp"))
@@ -81,16 +84,19 @@ macro(QTWEBKIT_GENERATE_MOC_FILE_H _header _source)
     endif ()
     get_filename_component(_name_we ${_header} NAME_WE)
     set(_moc_name "${CMAKE_CURRENT_BINARY_DIR}/moc_${_name_we}.cpp")
-    qt5_generate_moc(${_header} ${_moc_name})
+    qt5_generate_moc(${_header} ${_moc_name} TARGET ${_target})
     ADD_SOURCE_DEPENDENCIES(${_source} ${_moc_name})
 endmacro()
 
-macro(QTWEBKIT_GENERATE_MOC_FILES_H)
+macro(QTWEBKIT_GENERATE_MOC_FILES_H _target)
+    if (${ARGC} LESS 2)
+        message(FATAL_ERROR "QTWEBKIT_GENERATE_MOC_FILES_H must be called with at least 2 arguments")
+    endif ()
     foreach (_header ${ARGN})
         get_filename_component(_header_dir ${_header} DIRECTORY)
         get_filename_component(_name_we ${_header} NAME_WE)
         set(_source "${_header_dir}/${_name_we}.cpp")
-        QTWEBKIT_GENERATE_MOC_FILE_H(${_header} ${_source})
+        QTWEBKIT_GENERATE_MOC_FILE_H(${_target} ${_header} ${_source})
     endforeach ()
 endmacro()
 
-- 
2.15.1


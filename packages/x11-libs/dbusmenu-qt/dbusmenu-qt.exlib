# Copyright 2010 Alex Elsayed <eternaleye@gmail.com>
# Copyright 2010 Bo Ørsted Andresen
# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=lib${PN}
MY_PNV=${MY_PN}-${PV}

require launchpad [ pn=${MY_PN} branch=trunk suffix=tar.bz2 ] cmake [ api=2 ]

export_exlib_phases src_prepare

SUMMARY="Stand-alone library providing a way to import and export QMenu instances using the DBusMenu protocol"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc
"

# Tests require X to run
RESTRICT="test"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
    build+run:
        x11-libs/qtbase:5
"
#   Tests require qjson, but are useless currently; see note above
#   test:
#       dev-libs/qjson[>=0.5]

CMAKE_SOURCE=${WORKBASE}/\~dbusmenu-team/libdbusmenu-qt/trunk/

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PN}-optionaltests.patch )

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_DISABLE_FIND_PACKAGE_QJSON=TRUE
    -DUSE_QT5=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=( 'doc DOC' )

dbusmenu-qt_src_prepare() {
    cmake_src_prepare

    # Install doxygen docs to ${PNVR}/html instead of ${PN}
    edo sed -e "/DESTINATION/s:libdbusmenu-:${PNVR}/html/:" \
            -i CMakeLists.txt
}


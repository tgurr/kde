# Copyright 2015-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

SUMMARY="Applications useful for Plasma Development"
DESCRIPTION="
They can be used to create add-ons such as widgets, DataEngines and Runners
(search plugins) for Plasma applications as well as workspaces and contain:
- cuttlefish: An icon browser
- plasmaengineexplorer: Provides direct access to Plasma data engines
- plasmoidviewer: Plasmoid viewer shell to test Plasma applets
- themeexplorer: Explore and edit your Plasma themes
"

LICENCES="GPL-2"
SLOT="4"
MYOPTIONS=""

if ever at_least 5.14.90 ; then
    KF5_MIN_VER="5.54.0"
else
    KF5_MIN_VER="5.50.0"
fi
QT_MIN_VER="5.11.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
    run:
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        x11-libs/qtgraphicaleffects:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/${PN}-Disable-test-which-needs-X.patch
)


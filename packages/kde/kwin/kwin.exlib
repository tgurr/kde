# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require gtk-icon-cache

export_exlib_phases src_prepare

SUMMARY="An easy to use, but flexible, composited Window Manager"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] FDL-1.2 GPL-2"
SLOT="4"
MYOPTIONS="
    ( providers: eudev systemd ) [[
        *description = [ udev provider ]
        number-selected = exactly-one
    ]]
"

if ever at_least 5.14.90 ; then
    KF5_MIN_VER=5.54.0
else
    KF5_MIN_VER=5.50.0
fi
QT_MIN_VER=5.11.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        dev-libs/libepoxy
        kde/breeze:${SLOT}[>=5.9.0]   [[ note = [ possibly optional, default decoration ] ]]
        kde/kdecoration:${SLOT}[>=5.13.0]
        kde/kscreenlocker
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidletime:5[>=${KF5_MIN_VER}]
        kde-frameworks/kinit:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwayland:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        media-libs/fontconfig
        media-libs/freetype:2
        sys-libs/libcap
        sys-libs/libinput[>=1.9]
        sys-libs/wayland[>=1.2]
        x11-dri/libdrm[>=2.4.62]
        x11-dri/mesa[wayland]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libxcb[>=1.10]
        x11-libs/libXi
        x11-libs/libxkbcommon[>=0.7.0]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtscript:5[>=${QT_MIN_VER}]
        x11-libs/qtsensors:5[>=${QT_MIN_VER}]
        x11-libs/qttools:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        x11-utils/xcb-util-cursor
        x11-utils/xcb-util-image
        x11-utils/xcb-util-keysyms
        x11-utils/xcb-util-wm[>=0.4]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    run:
        kde-frameworks/kded:5
        x11-dri/mesa
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
    post:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
    suggestion:
        x11-libs/qtmultimedia:5[>=5.3.0] [[ description = [ Preview kwin effects as video ] ]]
        x11-libs/qtvirtualkeyboard:5 [[
            description = [ Integrate a virtual keyboard with kwin ]
        ]]
        x11-server/xorg-server[xwayland] [[ description = [ Needed for running kwin_wayland ] ]]
"

if ever at_least 5.14.90 ; then
    DEPENDENCIES+="
        run:
            kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
    "
fi

# 7 of 9 tests require a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_DISABLE_FIND_PACKAGE_libhybris:BOOL=TRUE
    -DKDE_DISTRIBUTION_TEXT:STRING="Exherbo"
    -DKWIN_BUILD_ACTIVITIES:BOOL=TRUE
)

kwin_src_prepare() {
    kde_src_prepare

    # Work around sydbox hanging after src_configure if
    # x11-libs/qtmultimedia[pulseaudio] is installed
    edo sed -e "/ecm_find_qmlmodule(QtMultimedia 5.0)/d" \
        -i CMakeLists.txt
}



Upstream: under review,  https://phabricator.kde.org/D19920

From 48feacf89819f46d84e0b8a334aac2010611915a Mon Sep 17 00:00:00 2001
From: Alexander Volkov <a.volkov@rusbitech.ru>
Date: Thu, 21 Mar 2019 11:51:38 +0100
Subject: [PATCH 1/2] Add High DPI support

Reviewers: #kde_games

Subscribers: apol, aacid, davidedmundson, kde-games-devel

Differential Revision: https://phabricator.kde.org/D19920
---
 libkcardgame/kabstractcarddeck.cpp | 25 ++++++++++++++++---------
 libkcardgame/kcard.cpp             |  2 +-
 libkcardgame/kcardthemewidget.cpp  |  9 +++++++--
 main.cpp                           |  2 ++
 4 files changed, 26 insertions(+), 12 deletions(-)

diff --git a/libkcardgame/kabstractcarddeck.cpp b/libkcardgame/kabstractcarddeck.cpp
index 214422d..1629c51 100644
--- a/libkcardgame/kabstractcarddeck.cpp
+++ b/libkcardgame/kabstractcarddeck.cpp
@@ -75,6 +75,7 @@ void RenderingThread::run()
         d->renderer();
     }
 
+    const auto size = m_size * qApp->devicePixelRatio();
     foreach ( const QString & element, m_elementsToRender )
     {
         {
@@ -83,11 +84,11 @@ void RenderingThread::run()
                 return;
         }
 
-        QString key = keyForPixmap( element, m_size );
+        QString key = keyForPixmap( element, size );
         if ( !d->cache->contains( key ) )
         {
             //qCDebug(LIBKCARDGAME_LOG) << "Renderering" << key << "in rendering thread.";
-            QImage img = d->renderCard( element, m_size );
+            QImage img = d->renderCard( element, size );
             d->cache->insertImage( key, img );
             emit renderingDone( element, img );
         }
@@ -197,24 +198,27 @@ QPixmap KAbstractCardDeckPrivate::requestPixmap( quint32 id, bool faceUp )
         return QPixmap();
 
     QPixmap & stored = it.value().cardPixmap;
-    if ( stored.size() != currentCardSize )
+    const auto dpr = qApp->devicePixelRatio();
+    QSize requestedCardSize = currentCardSize * dpr;
+    if ( stored.size() != requestedCardSize )
     {
-        QString key = keyForPixmap( elementId , currentCardSize );
+        QString key = keyForPixmap( elementId , requestedCardSize );
         if ( !cache->findPixmap( key, &stored ) )
         {
             if ( stored.isNull() )
             {
                 //qCDebug(LIBKCARDGAME_LOG) << "Renderering" << key << "in main thread.";
-                QImage img = renderCard( elementId, currentCardSize );
+                QImage img = renderCard( elementId, requestedCardSize );
                 cache->insertImage( key, img );
                 stored = QPixmap::fromImage( img );
             }
             else
             {
-                stored = stored.scaled( currentCardSize );
+                stored = stored.scaled( requestedCardSize );
             }
         }
-        Q_ASSERT( stored.size() == currentCardSize );
+        Q_ASSERT( stored.size() == requestedCardSize );
+        stored.setDevicePixelRatio( dpr );
     }
     return stored;
 }
@@ -235,16 +239,19 @@ void KAbstractCardDeckPrivate::submitRendering( const QString & elementId, const
 
     // If the currentCardSize has changed since the rendering was performed,
     // we sadly just have to throw it away.
-    if ( image.size() != currentCardSize )
+    const auto dpr = qApp->devicePixelRatio();
+    if ( image.size() != currentCardSize * dpr)
         return;
 
     // The RenderingThread just put the image in the cache, but due to the
     // volatility of the cache there's no guarantee that it'll still be there
     // by the time this slot is called, in which case we convert the QImage
     // passed in the signal.
-    if ( !cache->findPixmap( keyForPixmap( elementId, currentCardSize ), &pix ) )
+    if ( !cache->findPixmap( keyForPixmap( elementId, currentCardSize * dpr ), &pix ) )
         pix = QPixmap::fromImage( image );
 
+    pix.setDevicePixelRatio( dpr );
+
     QHash<QString,CardElementData>::iterator it;
     it = frontIndex.find( elementId );
     if ( it != frontIndex.end() )
diff --git a/libkcardgame/kcard.cpp b/libkcardgame/kcard.cpp
index cfaa1e8..fd95400 100644
--- a/libkcardgame/kcard.cpp
+++ b/libkcardgame/kcard.cpp
@@ -312,7 +312,7 @@ void KCard::paint( QPainter * painter, const QStyleOptionGraphicsItem * option,
     Q_UNUSED( option );
     Q_UNUSED( widget );
 
-    if ( pixmap().size() != d->deck->cardSize() )
+    if ( pixmap().size() != d->deck->cardSize() * pixmap().devicePixelRatio() )
     {
         QPixmap newPix = d->deck->cardPixmap( d->id, d->faceUp );
         if ( d->faceUp )
diff --git a/libkcardgame/kcardthemewidget.cpp b/libkcardgame/kcardthemewidget.cpp
index 08a27e0..65a918b 100644
--- a/libkcardgame/kcardthemewidget.cpp
+++ b/libkcardgame/kcardthemewidget.cpp
@@ -81,7 +81,9 @@ void PreviewThread::run()
                 return;
         }
 
-        QImage img( d->previewSize, QImage::Format_ARGB32 );
+        const auto dpr = qApp->devicePixelRatio();
+        QImage img( d->previewSize * dpr, QImage::Format_ARGB32 );
+        img.setDevicePixelRatio( dpr );
         img.fill( Qt::transparent );
         QPainter p( &img );
 
@@ -147,6 +149,7 @@ void CardThemeModel::reload()
     m_previews.clear();
 
     QList<KCardTheme> previewsNeeded;
+    const auto dpr = qApp->devicePixelRatio();
 
     foreach( const KCardTheme & theme, KCardTheme::findAllWithFeatures( d->requiredFeatures ) )
     {
@@ -157,8 +160,10 @@ void CardThemeModel::reload()
         QDateTime timestamp;
         if ( cacheFind( d->cache, timestampKey( theme ), &timestamp )
              && timestamp >= theme.lastModified() 
-             && d->cache->findPixmap( previewKey( theme, d->previewString ), pix ) )
+             && d->cache->findPixmap( previewKey( theme, d->previewString ), pix )
+             && pix->size() == d->previewSize * dpr )
         {
+            pix->setDevicePixelRatio( dpr );
             m_previews.insert( theme.displayName(), pix );
         }
         else
diff --git a/main.cpp b/main.cpp
index 53f0e45..554735d 100644
--- a/main.cpp
+++ b/main.cpp
@@ -101,6 +101,8 @@ QString lowerAlphaNum( const QString & string )
 
 int main( int argc, char **argv )
 {
+    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
+
     QApplication app(argc, argv);
 
     KLocalizedString::setApplicationDomain("kpat");
-- 
2.21.0


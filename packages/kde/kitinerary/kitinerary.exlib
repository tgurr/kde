# Copyright 2018-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

export_exlib_phases src_prepare

SUMMARY="A library containing a itinerary data model and itinerary extraction code"
DESCRIPTION="
This follows the reservation ontology from https://schema.org and Google's
extensions to it (https://developers.google.com/gmail/markup/reference/).
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    barcodes [[ description = [ Support for decoding barcodes ] ]]
    pdf-extractor [[ description = [ Extract information from PDF booking confirmations ] ]]
"

if ever at_least 19.03.80 ; then
    MYOPTIONS+="
        phone-numbers [[ description = [ Parsing and geo-coding of phone numbers ] ]]
    "

    KF5_MIN_VER="5.56.0"
    QT_MIN_VER="5.10.0"
else
    KF5_MIN_VER="5.51.0"
    QT_MIN_VER="5.9.0"
fi

DEPENDENCIES="
    build:
        dev-libs/libxslt [[ note = [ xlstproc ] ]]
    build+run:
        dev-libs/libxml2:2.0
        kde/kpkpass
        kde-frameworks/kcalcore:5[>=${PV}]
        kde-frameworks/kcontacts:5[>=${PV}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmime:5[>=${PV}]
        sys-libs/zlib
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        barcodes? ( media-libs/zxing-cpp )
        pdf-extractor? ( app-text/poppler )
"

if ever at_least 19.03.80 ; then
    DEPENDENCIES+="
        build+run:
            dev-libs/libphonenumber
    "
    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'phone-numbers PhoneNumber' )
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'barcodes ZXing'
    'pdf-extractor Poppler'
)

kitinerary_src_prepare() {
    kde_src_prepare

    # Disable test which needs X and is meaningless without external, private
    # data anyway.
    edo sed -e "/ecm_add_test(extractortest.cpp /d" \
        -i autotests/CMakeLists.txt
}


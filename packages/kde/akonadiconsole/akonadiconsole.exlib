# Copyright 2016-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde
require gtk-icon-cache

export_exlib_phases src_prepare

SUMMARY="Akonadi Management and Debugging Console"

LICENCES="FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

if ever at_least 19.03.80 ; then
    KF5_MIN_VER=5.56.0
    QT_MIN_VER=5.10.0
else
    KF5_MIN_VER=5.51.0
    QT_MIN_VER=5.9.0
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        dev-db/xapian-core
        kde/calendarsupport[>=${PV}]
        kde/libkdepim[>=${PV}]
        kde/messagelib[>=${PV}]
        kde-frameworks/akonadi-contact:5[>=${PV}]
        kde-frameworks/akonadi-search:5[>=${PV}]
        kde-frameworks/kcalcore:5[>=${PV}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=${PV}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui][sql]
        !kde/kdepim[<16.07.80] [[
            description = [ akonadiconsole has been split out from kdepim ]
            resolution = uninstall-blocked-after
        ]]
"

akonadiconsole_src_prepare() {
    cmake_src_prepare

    # Disable test which needs X
    edo sed -e "/add_unittest(jobtrackersearchwidgettest.cpp/d" \
            -i autotests/CMakeLists.txt
}


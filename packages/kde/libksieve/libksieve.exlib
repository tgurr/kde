# Copyright 2016-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

SUMMARY="A library managing sieve support"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    GPL-2 LGPL-2.1
"
SLOT="0"
MYOPTIONS=""

if ever at_least 19.03.80 ; then
    KF5_MIN_VER=5.56.0
    QT_MIN_VER=5.10.0
else
    KF5_MIN_VER=5.51.0
    QT_MIN_VER=5.9.0
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        net-libs/cyrus-sasl
        kde/libkdepim[>=${PV}]
        kde/pimcommon[>=${PV}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidentitymanagement:5[>=${PV}]
        kde-frameworks/kimap:5[>=${PV}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmailtransport:5[>=${PV}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpimtextedit:5[>=${PV}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        kde-frameworks/syntax-highlighting:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
        !kde/kdepim[<16.03.80] [[
            description = [ libksieve has been split out from kdepim ]
            resolution = uninstall-blocked-after
        ]]
        !kde/kdepim-kioslaves:4 [[
            description = [ The sieve kioslave has been moved to this package ]
            resolution = uninstall-blocked-after
        ]]
"

# 9 of 11 tests need a running X server
RESTRICT="test"


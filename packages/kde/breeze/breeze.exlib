# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2015-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require gtk-icon-cache

SUMMARY="Artwork, styles and assets for the Breeze style for the Plasma Desktop"

LICENCES="GPL-2 LGPL-3 [[ note = [ icons ] ]]"
SLOT="4"
# wallpapers need 22 of 28 MB here, a bit much if you only want the style.
MYOPTIONS="
    wallpapers [[ description = [ Installs the Breeze default wallpapers ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde/kdecoration:${SLOT} [[ note = [ could be optional with 5.6.x ] ]]
        kde-frameworks/frameworkintegration:5 [[ note = [ provides KF5Style ] ]]
        kde-frameworks/kcmutils:5
        kde-frameworks/kconfig:5
        kde-frameworks/kconfigwidgets:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/kguiaddons:5
        kde-frameworks/ki18n:5
        kde-frameworks/kpackage:5
        kde-frameworks/kwayland:5
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kwindowsystem:5
        kde-frameworks/plasma-framework:5
        x11-libs/libxcb
        x11-libs/qtbase:5[>=5.2.0]
        x11-libs/qtdeclarative:5[>=5.2.0]
        x11-libs/qtx11extras:5[>=5.2.0]
    run:
        kde-frameworks/breeze-icons:5
        x11-libs/qtgraphicaleffects:5[>=5.2.0]
        x11-libs/qtquickcontrols:5[>=5.2.0]
    post:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
    suggestion:
        x11-themes/breeze-gtk [[ description = [ Matching theme for gtk+:2 and gtk+:3 ] ]]
"

if ever at_least 5.14.90 ; then
    :
else
    DEPENDENCIES+="
        build+run:
            sci-libs/fftw
    "
fi

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DUSE_KDE4:BOOL=FALSE
    -DWITH_DECORATIONS:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_WITHS+=( wallpapers )


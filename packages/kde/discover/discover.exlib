# Copyright 2015-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations=ki18n ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="KDE and Plasma resources management GUI"

LICENCES="FDL-1.2 GPL-3 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    firmware [[ description = [ Allows installation of firmware updates ] ]]
    flatpak  [[ description = [ Build a backend to handle Flatpak applications ] ]]
"

if ever at_least 5.14.90 ; then
    KF5_MIN_VER="5.54.0"
else
    KF5_MIN_VER="5.50.0"
fi
QT_MIN_VER="5.11.0"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/attica:5[>=${KF5_MIN_VER}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}] [[ note = [ could be optional ] ]]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        firmware? (
            dev-libs/glib:2
            sys-apps/fwupd[>=1.0.6]
        )
        flatpak? (
            dev-libs/appstream[>=0.11.1][qt5]
        )
    run:
        kde-frameworks/kirigami:2[>=2.1.0]
        x11-libs/qtgraphicaleffects:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
"

if ever at_least 5.14.90 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
            flatpak? ( sys-apps/flatpak[>=0.11.8] )
    "
else
    DEPENDENCIES+="
        build+run:
            flatpak? ( sys-apps/flatpak[>=0.6.12] )
    "
fi

# 1 of 1 test needs a running X server (last checked: 5.5.0)
RESTRICT="test"

# TODO: Only search for libmarkdown for in the packagekit part
CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_DummyBackend:BOOL=FALSE
    -DCMAKE_DISABLE_FIND_PACKAGE_packagekitqt5:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Snapd:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=(
    'firmware FwupdBackend'
    'flatpak FlatpakBackend'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'flatpak AppStreamQt' )

discover_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

discover_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}


# Copyright 2011,2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdeutils.exlib', which is:
#     Copyright 2008-2011 Bo Ørsted Andresen

require kde-apps kde [ translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache
require test-dbus-daemon

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="Graphical interface for GnuPG"
HOMEPAGE="http://utils.kde.org/projects/${PN}/"
DESCRIPTION="
With KGpg you will be able to encrypt and decrypt your files and emails,
allowing much more secure communications. A mini howto on encryption with gpg
is available on gnupg's web site.
"

LICENCES="GPL-2"
MYOPTIONS=""

KF5_MIN_VER=5.28.0

DEPENDENCIES="
    build:
        app-crypt/gpgme [[ note = [ only headers are used ] ]]
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/akonadi-contact:5
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.5.0][gui]
    run:
        app-crypt/gnupg
"

kgpg_src_test() {
    esandbox allow_net "unix:${TEMP}/kgpg-*/.gnupg/S.gpg-agent"
    esandbox allow_net --connect "unix:${TEMP}/kgpg-*/.gnupg/S.gpg-agent"
    esandbox allow_net "unix:${TEMP}/kgpg-*/.gnupg/S.gpg-agent.browser"
    esandbox allow_net "unix:${TEMP}/kgpg-*/.gnupg/S.gpg-agent.extra"
    esandbox allow_net "unix:${TEMP}/kgpg-*/.gnupg/S.gpg-agent.ssh"

    test-dbus-daemon_run-tests

    esandbox disallow_net "unix:${TEMP}/kgpg-*/.gnupg/S.gpg-agent.ssh"
    esandbox disallow_net "unix:${TEMP}/kgpg-*/.gnupg/S.gpg-agent.extra"
    esandbox disallow_net "unix:${TEMP}/kgpg-*/.gnupg/S.gpg-agent.browser"
    esandbox disallow_net --connect "unix:${TEMP}/kgpg-*/.gnupg/S.gpg-agent"
    esandbox disallow_net "unix:${TEMP}/kgpg-*/.gnupg/S.gpg-agent"
}

kgpg_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kgpg_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}


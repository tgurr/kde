# Copyright 2013 Arne Janbu <devel@arnej.de>
# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

SUMMARY="Plasma applet for managing network connections"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] GPL-2 LGPL-2.1"
SLOT="4"
MYOPTIONS="
   modem-manager [[ description = [ Support managing of networking via ModemManager ] ]]
   openconnect   [[ description = [ Support Cisco's AnyConnect SSL VPN ] ]]
   ( linguas:
        ar bg bs ca ca@valencia cs da de el en_GB eo es et fa fi fr ga gl hr
        hu ia is it ja km ko lt lv mai mr ms nb nds nl nn pa pl pt pt_BR ro ru
        sk sl sr sr@ijekavian sr@ijekavianlatin sr@latin sv th tr ug uk zh_CN
        zh_TW
    )
"

if ever at_least 5.15.0 ; then
    KF5_MIN_VER="5.54.0"
else
    KF5_MIN_VER="5.50.0"
fi
QT_MIN_VER="5.11.0"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-crypt/qca[>=2.1.0][qt5(+)]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        kde-frameworks/networkmanager-qt:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        net-apps/NetworkManager[>=1.4.0]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        modem-manager? (
            kde-frameworks/modemmanager-qt:5[>=${KF5_MIN_VER}]
            net-wireless/ModemManager[>=1.0.0]
        )
        openconnect? ( net-apps/openconnect[>=3.99] )
        !kde/plasma-nm:0 [[
            description = [ Collides with its pre Plasma 5 counterpart ]
            resolution = uninstall-blocked-after
        ]]
    run:
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
    recommendation:
        net-misc/mobile-broadband-provider-info [[
            description = [ Database of mobile broadband service providers ]
        ]]
"

if ever at_least 5.15.0 ; then
    DEPENDENCIES+="
        suggestion:
            net-apps/NetworkManager-wireguard [[
                description = [ Provides support for WireGuard VPN tunnels ]
            ]]
    "
fi

BUGS_TO="devel@arnej.de"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'modem-manager ModemManager'
    'modem-manager KF5ModemManagerQt'
    OpenConnect
)


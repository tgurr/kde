# Copyright 2013-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require gtk-icon-cache

SUMMARY="KDE's screen management software"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

if ever at_least 5.14.90 ; then
    KF5_MIN_VER="5.54.0"
else
    KF5_MIN_VER="5.50.0"
fi
QT_MIN_VER="5.11.0"

DEPENDENCIES="
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/libkscreen:5[>=5.2.91]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
    run:
        kde/kde-cli-tools:4   [[ note = [ kcmshell5 ] ]]
        x11-libs/qtgraphicaleffects:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
"

# Tests need a running X server (1.0.2)
RESTRICT=test


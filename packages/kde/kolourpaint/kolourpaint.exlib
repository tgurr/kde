# Copyright 2011 Johannes Nixdorf <mixi@exherbo.org>
# Copyright 2016-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="A simple paint program"

LICENCES="BSD-2 FDL-1.2 LGPL-2"
MYOPTIONS="
    scanner [[ description = [ Support for scanning via libksane ] ]]
"

QT_MIN_VER="5.5.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5
    build+run:
        kde-frameworks/kdelibs4support:5
        kde-frameworks/kguiaddons:5
        kde-frameworks/ki18n:5
        kde-frameworks/kiconthemes:5
        kde-frameworks/kio:5
        kde-frameworks/ktextwidgets:5
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kxmlgui:5
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        scanner? ( kde/libksane:5 )
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'scanner KF5Sane' )

kolourpaint_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kolourpaint_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}


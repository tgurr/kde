# Copyright 2015-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ] gtk-icon-cache

SUMMARY="Screenshot taking utility"

LICENCES="FDL-1.3 GPL-2"
SLOT="0"
MYOPTIONS="
    kipi  [[ description = [ Provides various image manipulation and export features via KDE Image Plugin Interface (KIPI) ] ]]
    share [[ description = [ Share screenshots via web services, IM, KDEConnect, etc. ] ]]
"

KF5_MIN_VER=5.29.0

QT_MIN_VER=5.6.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/libkscreen:5[>=5.4.0]
        x11-libs/libxcb
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        x11-utils/xcb-util
        x11-utils/xcb-util-cursor
        x11-utils/xcb-util-image
        kipi? ( kde/libkipi:5 )
        share? ( kde-frameworks/purpose:5 )
    run:
        kipi? ( graphics/kipi-plugins[>=5.0.0-beta] )
    suggestion:
        kde/kwin [[ description = [ Needed for taking screenshots under Wayland ] ]]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'kipi KF5Kipi'
    'share KDEExperimentalPurpose'
)


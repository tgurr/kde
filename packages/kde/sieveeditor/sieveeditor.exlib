# Copyright 2016 Niels Ole Salscheider <olesalscheider@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps [ pn='pim-sieve-editor' ] kde [ translations='ki18n' ]

SUMMARY="KDE application to edit sieve scripts"

LICENCES="FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

if ever at_least 19.03.80 ; then
    KF5_MIN_VER=5.56.0
    QT_MIN_VER=5.10.0
else
    KF5_MIN_VER=5.51.0
    QT_MIN_VER=5.9.0
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde/libksieve[>=${PV}]
        kde/pimcommon[>=${PV}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kimap:5[>=${PV}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmailtransport:5[>=${PV}]
        kde-frameworks/kpimtextedit:5[>=${PV}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        !kde/kdepim [[
            description = [ sieveeditor has been split out from kdepim ]
            resolution = uninstall-blocked-after
        ]]
    suggestion:
        kde/kdepim-addons [[
            description = [ Provides a plugin for IMAP folder completion ]
        ]]
"

# 2 of 3 tests need a running X server
RESTRICT="test"


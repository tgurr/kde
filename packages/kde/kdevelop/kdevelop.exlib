# Copyright 2009 Yury G. Kudryashov <urkud@ya.ru>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/${PV}/src" ] kde
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases src_prepare pkg_postinst pkg_postrm

SUMMARY="KDE official IDE"
DESCRIPTION="
A free, opensource IDE (Integrated Development Environment) for MS Windows, Mac OsX, Linux, Solaris
and FreeBSD. It is a feature-full, plugin extendable IDE for C/C++ and other programing languages.
It is based on KDevPlatform, KDE and Qt libraries and is under development since 1998."
HOMEPAGE="https://www.kdevelop.org/"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    GPL-2 LGPL-2 FDL-1.2
"
SLOT="4"

MYOPTIONS="
    okteta [[ description = [ Build okteta hex editor plugin ] ]]
    patch-sharing [[ description = [ Support for sharing patches with kde-frameworks/purpose ] ]]
    plasma [[ description = [ Enables some KDevelop related plasmoids and runners ] ]]
    qmake  [[ description = [ Build the QMake Builder/Manager plugin ] ]]
    subversion [[ description = [ Support for Subversion integration ] ]]
"

KF5_MIN_VER=5.15.0
QT_MIN_VER=5.5.0

DEPENDENCIES="
    build:
        dev-libs/boost[>=1.35.0]
        sys-devel/gettext
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        dev-lang/clang:=[>=3.8] [[ note = [ There's also a legacy C++ backend ] ]]
        dev-lang/llvm:=
        dev-libs/grantlee:5
        kde/libkomparediff2:4[>=15.03.80]
        kde/libksysguard   [[ note = [ Provides a dialog to Attach gdb to a running process ] ]]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}] [[ note = [ kcoreaddons_desktop_to_json ] ]]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        kde-frameworks/threadweaver:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qttools:5[>=${QT_MIN_VER}]   [[ note = [ QtHelp ] ]]
        x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info
        okteta? ( kde/okteta:${SLOT}[>=0.25.0] )
        patch-sharing? ( kde-frameworks/purpose:5 )
        plasma? (
            kde-frameworks/krunner:5
            kde-frameworks/plasma-framework:5
        )
        qmake? ( kde/kdevelop-pg-qt[>=1.90.90] )
        subversion? ( dev-scm/subversion[>=1.3.0] )
        !kde/kdevplatform [[
            description = [ kde/kdevplatform was merged into kdevelop ]
            resolution = uninstall-blocked-after
        ]]
    run:
        kde/kate:${SLOT}[>=14.12.0]
    suggestion:
        dev-scm/git [[ description = [ Use Git from within KDevelop ] ]]
        dev-scm/subversion [[ description = [ Use Subversion from within KDevelop ] ]]
        dev-util/clazy [[ description = [ Plugin to check and analyze code with Clazy ] ]]
        dev-util/cppcheck [[ description = [ Plugin for static C/C++ code anylsis ] ]]
        dev-util/heaptrack [[ description = [ Plugin for heap memory profiling ] ]]
        kde/kdev-php [[ description = [ KDevelop plugin for PHP development ] ]]
        kde/kdev-python [[ description = [ KDevelop plugin for Python 3 development ] ]]
        sys-devel/gdb[>=7.0][python] [[ description = [ The only supported debugger ] ]]
        sys-devel/ninja [[ description = [ Use ninja to build your projects ] ]]
    test:
        dev-scm/git
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    "okteta KastenControllers"
    'okteta OktetaGui'
    "okteta OktetaKastenControllers"
    'patch-sharing KF5Purpose'
    'patch-sharing KDEExperimentalPurpose'
    'plasma KF5Plasma'
    'plasma KF5Runner'
    'qmake KDevelop-PG-Qt'
    'subversion SubversionLibrary'
)

# tests fail due dbus and X11, test-dbus-daemon did not help.
RESTRICT="test"

kdevelop_src_prepare() {
    cmake_src_prepare

    # Disable tests needing a running X server
    edo sed -e "/kdevcmake_add_test(cmakeloadprojecttest/d" \
            -e "/kdevcmake_add_test(cmakemanagertest/d" \
            -i plugins/cmake/tests/CMakeLists.txt
    edo sed -e "/add_subdirectory( tests )/d" \
            -i plugins/custom-buildsystem/CMakeLists.txt
}

kdevelop_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kdevelop_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}


# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/$(ever range -3)/src" suffix=tar.gz ] kde
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases src_install pkg_postinst pkg_postrm

SUMMARY="An easy to use and easily extensible IDE/GUI for R"
DESCRIPTION="
It aims to combine the power of the R-language with the ease of use of
commercial statistics tools.
RKWard's features include:
- Spreadsheet-like data editor
- Syntax highlighting, code folding and code completion
- Data import (e.g. SPSS, Stata and CSV)
- Plot preview and browsable history
- R package management
- Workspace browser
- GUI dialogs for all kinds of statistics and plots
Its features can be extended by plugins, and it's all free software.
"

HOMEPAGE="https://rkward.kde.org/"

LICENCES="GPL-2 LGPL-2"
SLOT="0"
MYOPTIONS=""

if ever at_least scm ; then
    KF5_MIN_VER="5.4"
else
    KF5_MIN_VER="5.2"
fi
QT_MIN_VER="5.2"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        sys-devel/gettext
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdewebkit:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        sci-lang/R[>=2.10.0]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtscript:5[>=${QT_MIN_VER}]
        x11-libs/qtwebkit:5[>=${QT_MIN_VER}]
"

BUGS_TO="heirecka@exherbo.org"

CMAKE_SRC_CONFIGURE_PARAMS=( "-DAPPLE_STANDALONE_BUNDLE:BOOL=FALSE" )

rkward_src_install() {
    cmake_src_install

    if ever at_least scm ; then
        :
    else
        # Empty because untranslated (0.7.0)
        edo rm -rf "${IMAGE}"/usr/share/rkward/po/{ast,bs,ca@valencia,cs,de,fi,fr,ja,lt,sk,zh_CN}
        edo rm -rf "${IMAGE}"/usr/share/locale/{ast,bs,cs,da,el,fi,fr,it,ja,lt,sk,zh_CN}
    fi
}

rkward_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

rkward_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

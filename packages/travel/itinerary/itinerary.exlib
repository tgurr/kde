# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde freedesktop-desktop

export_exlib_phases src_prepare

SUMMARY="Itinerary and boarding pass management application"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS=""

# Because of QuickCompiler which became open source with that version (or at
# least I think so)
QT_MIN_VER="5.11.0"

DEPENDENCIES="
    build+run:
        kde/kitinerary[>=18.11.80]
        kde/kpkpass[>=18.11.80]
        kde-frameworks/kcontacts:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/kdbusaddons:5
        kde-frameworks/ki18n:5
        kde-frameworks/kholidays:5[>=5.49.0]
        kde-frameworks/knotifications:5[>=5.49.0]
        kde/kpublictransport
        sys-libs/zlib
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtlocation:5[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info[>=1.0]
    run:
        kde-frameworks/prison:5
        kde-frameworks/solid:5 [[ note =
            [ Used for controlling the screen brightness ]
        ]]
        x11-libs/qtquickcontrols2:5 [[ note = [ Qt.labs.platform ] ]]
"

itinerary_src_prepare() {
    kde_src_prepare

    # Disable test which wants to access the systemd dbus
    edo sed -e "/weathertest/d" -i autotests/CMakeLists.txt
}


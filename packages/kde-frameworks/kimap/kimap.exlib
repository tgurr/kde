# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require test-dbus-daemon

SUMMARY="A job-based API for interacting with IMAP servers"
DESCRIPTION="
This library provides a job-based API for interacting with an IMAP4rev1
server. It manages connections, encryption and parameter quoting and encoding,
but otherwise provides quite a low-level interface to the protocol. This
library does not implement an IMAP client; it merely makes it easier to do so."

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] GPL-2 LGPL-2.1"
SLOT="5"
MYOPTIONS=""

if ever at_least 19.03.80 ; then
    KF5_MIN_VER=5.56.0
    QT_MIN_VER=5.10.0
else
    KF5_MIN_VER=5.51.0
    QT_MIN_VER=5.9.0
fi

DEPENDENCIES="
    build+run:
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmime:5[>=${PV}]
        net-libs/cyrus-sasl
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
"


# Copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks [ docs=false ] kde [ translations='ki18n' ]

SUMMARY="A web rendering engine, based on KParts and using KJS for JavaScript"

LICENCES="GPL-3 LGPL-2.1"
MYOPTIONS="
    X [[ presumed = true ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-util/gperf
    build+run:
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjs:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        media-libs/giflib:=
        media-libs/libpng:=
        media-libs/phonon[>=4.6.60][qt5(+)]
        sys-libs/zlib
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        X? (
            x11-libs/libX11
            x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        )
    suggestion:
        kde/kde-cli-tools:4 [[
            description = [ Needed to launch the proxy KC(ontrol)M(odule) ]
        ]]
"

# 2 of 2 tests need a running X server (5.0.0)
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'X X11' )


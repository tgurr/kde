# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde

SUMMARY="Utilities for graphical user interfaces"
DESCRIPTION="
The KDE GUI addons provide utilities for graphical user interfaces in the areas
of colors, fonts, text, images, keyboard input."

LICENCES="GPL-2 LGPL-2.1"
MYOPTIONS="
    X [[ presumed = true ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        X? (
            x11-libs/libX11
            x11-libs/libxcb
            x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        )
"

# 3 of 3 tests need a running X server (5.0.0)
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Needed to build python bindings, no released consumers and we'd need
    # clang's python bindings and fix their and libclang's detection in the
    # cmake module, thus we disable it for now. Would need python,
    # clang[>=3.8], sip and PyQt5.
    -DCMAKE_DISABLE_FIND_PACKAGE_PythonModuleGeneration:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'X Qt5X11Extras'
    'X XCB'
)


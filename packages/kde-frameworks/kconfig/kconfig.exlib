# Copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='qt' ]

SUMMARY="Provides an advanced configuration system"
DESCRIPTION="
It is made of two parts: KConfigCore and KConfigGui.
KConfigCore provides access to the configuration files themselves. It features:
- centralized definition: define your configuration in an XML file and use
\`kconfig_compiler\` to generate classes to read and write configuration entries.
- lock-down (kiosk) support.
KConfigGui provides a way to hook widgets to the configuration so that they are
automatically initialized from the configuration and automatically propagate
their changes to their respective configuration files."

LICENCES="BSD-3 [[ note = [ cmake macro ] ]] LGPL-2.1 "
MYOPTIONS=""

DEPENDENCIES=""

# Many tests need a running X server, others also fail probably because of
# the paludisbuild environment. (5.10.0)
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Needed to build python bindings, no released consumers and we'd need
    # clang's python bindings and fix their and libclang's detection in the
    # cmake module, thus we disable it for now. Would need python,
    # clang[>=3.8], sip and PyQt5.
    -DCMAKE_DISABLE_FIND_PACKAGE_PythonModuleGeneration:BOOL=TRUE
    -KCONFIG_USE_DBUS:BOOL=TRUE
    -KCONFIG_USE_GUI:BOOL=TRUE
)


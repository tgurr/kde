# Copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde

SUMMARY="Library to organize the user work in separate activities"
DESCRIPTION="
When a user is interacting with a computer, there are three main areas of contextual information
that may affect the behaviour of the system: who the user is, where they are, and what they are
doing.
*Activities* deal with the last one. An activity might be 'developing a KDE application',
'studying 19th century art', 'composing music' or 'watching funny videos'. Each of these activites
may involve multiple applications, and a single application may be used in multiple activities
(for example, most activities are likely to involve using a web browser, but different activities
will probably involve different websites).
KActivities provides the library part of the infrastructure needed to manage a user's activites,
allowing them to switch between tasks, and for applications to update their state to match the
user's current activity. The daemon and plugins for integration with other frameworks are found
in suggested packages."

LICENCES="GPL-2 LGPL-2 LGPL-2.1"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/boost[>=1.49]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui][sql][sqlite]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
    suggestion:
        kde/kio-extras:4[>=15.12.2-r1] [[
            description = [ Plugin to link files to activities ]
        ]]
        kde/kactivitymanagerd [[
            description = [ Runtime component to manage and switch activities ]
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_AUTOMOC:BOOL=TRUE
    -DKACTIVITIES_LIBRARY_ONLY:BOOL=FALSE
)


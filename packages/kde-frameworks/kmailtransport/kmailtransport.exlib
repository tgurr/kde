# Copyright 2015 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

SUMMARY="Framework for managing mail transports"

LICENCES="LGPL-2.1"
SLOT="5"
MYOPTIONS=""

if ever at_least 19.03.80 ; then
    KF5_MIN_VER=5.56.0
    QT_MIN_VER=5.10.0
else
    KF5_MIN_VER=5.51.0
    QT_MIN_VER=5.9.0
fi

DEPENDENCIES="
    build+run:
        app-pim/libkgapi[>=${PV}]
        kde/ksmtp[>=${PV}]
        kde-frameworks/akonadi-mime:5[>=${PV}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        !kde/kdepim-kioslaves:4 [[
            description = [ The smtp kioslave has been moved to this package ]
            resolution = uninstall-blocked-after
        ]]
        !kde/kdepimlibs:4[<4.14.10-r4] [[
            description = [ Collides with its old KDELibs4 counterpart ]
            resolution = uninstall-blocked-after
        ]]
    test:
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
"

# 3 of 3 tests need a running X server
RESTRICT="test"


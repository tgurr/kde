# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='ki18n' ] test-dbus-daemon

SUMMARY="Safe desktop-wide storage for passwords"
DESCRIPTION="
This framework contains two main components:
* Interface to KWallet, the safe desktop-wide storage for passwords on KDE work
spaces.
* The kwalletd used to safely store the passwords on KDE work spaces."

LICENCES="LGPL-2.1"
MYOPTIONS="
    gpg [[ description = [ Adds a GPG backend in addition to the blowfish one ] ]]
"

# The library can be built alone, without kwalletd, by setting BUILD_KWALLETD,
# but that's probably only reasonable if it's used outside of KDE.
DEPENDENCIES="
    build:
        doc? ( kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}] )
    build+run:
        dev-libs/libgcrypt[>=1.5.0]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        gpg? (
            app-crypt/gpgme[>=1.7.0]
        )
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # small command-line tool with no additional deps
    -DBUILD_KWALLET_QUERY:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'doc KF5DocTools'
    'gpg Gpgmepp'
    'gpg Gpgme'
)


# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2010 Ingmar Vanhassel
# Copyright 2013-2014, 2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'phonon-4.2-scm.kdebuild-1' from Genkdesvn, which is:
#     Copyright 2007-2008 by the individual contributors of the genkdesvn project
#     Based in part upon the respective ebuild in Gentoo which is:
#     Copyright 1999-2008 Gentoo Foundation

require kde.org cmake [ api=2 ]

SUMMARY="KDE multimedia API"
HOMEPAGE="https://phonon.kde.org/"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2.1"
SLOT="0"
MYOPTIONS="doc
    gstreamer  [[ description = [ Enable the Phonon Gstreamer backend ] ]]
    pulseaudio [[ description = [ Enable playback through Pulseaudio ] ]]
    vlc        [[ description = [ Use the Phonon VLC backend, which is the recommended backend ] ]]

    ( gstreamer vlc ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
        kde-frameworks/extra-cmake-modules[>=1.7.0]
        virtual/pkg-config
    build+run:
        x11-libs/qtbase:5
        x11-libs/qttools:5
        pulseaudio? (
            dev-libs/glib:2
            media-sound/pulseaudio[>=0.9.21]
        )
    post:
        gstreamer? ( media-libs/phonon-gstreamer )
        vlc? ( media-libs/phonon-vlc )
"

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'doc PHONON_BUILD_DOC'
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
        'pulseaudio GLIB2'
        'pulseaudio PulseAudio'
)
CMAKE_SRC_CONFIGURE_PARAMS=(
    # automoc is included in cmake since 2.8.5
    -DCMAKE_AUTOMOC:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Declarative:BOOL=TRUE
    -DCMAKE_INSTALL_DATAROOTDIR:PATH=/usr/share
    # super experimental QML support
    -DPHONON_BUILD_DECLARATIVE_PLUGIN:BOOL=FALSE
    -DPHONON_BUILD_DESIGNER_PLUGIN:BOOL=TRUE
    -DPHONON_BUILD_PHONON4QT5:BOOL=TRUE \
    -DPHONON_INSTALL_QT_EXTENSIONS_INTO_SYSTEM_QT:BOOL=TRUE
)


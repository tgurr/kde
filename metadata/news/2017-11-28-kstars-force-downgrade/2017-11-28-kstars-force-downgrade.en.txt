Title: kstars update needs --permit-downgrade
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2017-11-28
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde/kstars[>=4]

kde/kstars is no longer released with KDE Applications because the monthly
releases apparently were incompatible with kstar's development [1]. Because of
this its version changed from yy.mm.i (e.g. 17.08.3) to currently 2.8.8,
requiring you to pass "--permit-downgrade" to your cave invocation when
updating:

# cave resolve -1 kde/kstars --permit-downgrade kde/kstars.

[1]: https://mail.kde.org/pipermail/release-team/2017-November/010687.html

Title: kde/kdevelop-php has been renamed to kde/kdev-php
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2017-09-03
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde/kdevelop-php

Please install kde/kdev-php and *afterwards* uninstall kde/kdevevelop-php.

1. Take note of any packages depending on kde/kdevelop-php
cave resolve \!kde/kdevelop-php

2. Install kde/kdev-php:
cave resolve kde/kdev-php -x

3. Re-install the packages from step 1.

4. Uninstall kde/kdevelop-php
cave resolve \!kde/kdevelop-php -x

Do it in *this* order or you'll potentially *break* your system.
